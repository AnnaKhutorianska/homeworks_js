let number1 = "";
let number2 = "";

do {
  number1 = +prompt("Input number1 > 1", number1);
  number2 = +prompt("Input number2 > 1", number2);
} while (
  !Number.isInteger(Number.parseFloat(number1)) ||
  number1 <= 1 ||
  !Number.isInteger(Number.parseFloat(number2)) ||
  number2 <= 1 ||
  number2 <= number1
);

for (let i = number1; i <= number2; i++) {
  flag = false;
  for (let j = 2; j < i; j++) {
    if (i % j === 0) {
      flag = true;
      break;
    }
  }

  if (!flag) {
    console.log(i);
  }
}
