let number = "";

do {
  number = prompt("Input number", number);
} while (!Number.isInteger(Number.parseFloat(number)) || number <= 0);

let flag = false;

for (let i = 0; i <= number; i++) {
  if (i % 5 === 0) {
    console.log(i);
    flag = true;
  }
}

if (!flag) {
  console.log("Sorry, no numbers");
}
