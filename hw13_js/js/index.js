const changeThemeBtn = document.querySelector('#changeTheme');
const body = document.querySelector('body');

window.addEventListener('load', () => {
	const mode = localStorage.getItem('mode');

	if (mode) {
		body.classList.replace(body.classList, mode);
	}
});

changeThemeBtn.addEventListener('click', () => {
	if (body.classList.contains('light')) {
		body.classList.replace('light', 'dark');
		localStorage.setItem('mode', 'dark');
		return;
	}
	
	body.classList.replace('dark', 'light');
	localStorage.setItem('mode', 'light');
});
