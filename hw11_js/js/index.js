const buttons = document.querySelectorAll('.btn');

const checkLetter = (e) => {
	buttons.forEach((btn) => {
		btn.classList.remove('btn-pressed');
		if (e.key.toLowerCase() === btn.dataset.key) {
			btn.classList.add('btn-pressed');
		}
	});
};

window.addEventListener('keydown', checkLetter);
