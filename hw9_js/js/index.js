const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelectorAll('.content');

tabs.addEventListener('click', (e) => {
	const tabsTitle = tabs.querySelectorAll('.tabs-title');

	tabsTitle.forEach((tabTitle) => {
		tabTitle.classList.remove('active');
	});

	e.target.classList.add('active');
	let tabAttr = e.target.getAttribute('data-content');

    showContent(tabAttr);
});

const showContent = ((attr) => {
    tabsContent.forEach(tabContent => {
        if(tabContent.getAttribute('data-content') === attr){
            tabContent.classList.remove('content');
        } else {
            tabContent.classList.add('content');
        }
    });
});
