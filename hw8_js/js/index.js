let inputPrice = document.createElement('input');
inputPrice.placeholder = 'Price';
document.body.append(inputPrice);
const errorMessage = document.createElement('span');
const list = document.createElement('ul');
list.classList.add('list');

const focusFunc = (e) => {
	e.target.classList.remove('invalide-price','input-text');
	e.target.classList.add('on-focus');
    errorMessage.remove();
	e.target.value = '';
};

const blurFunc = (e) => {
	e.target.classList.remove('on-focus');
	checkValue();
};

const checkValue = () => {
	if (inputPrice.value < 0 || inputPrice.value === '' || Number.isNaN(parseFloat(inputPrice.value))) {
        inputPrice.classList.add('invalide-price');
		errorMessage.innerText = '\nPlease enter correct price';
		inputPrice.after(errorMessage);
	} else {
		inputPrice.classList.add('input-text');
		inputPrice.before(list);
		const li = document.createElement('li');
		li.classList.add('valide-price');
		const deleteBtn = createDelButton();
		li.innerText = `Текущая цена: ${inputPrice.value}`;
		li.append(deleteBtn);
		list.append(li);
	}
};

const createDelButton = () => {
	const delBtn = document.createElement('button');
	delBtn.innerText = 'X';
	delBtn.addEventListener('click', (e) => {
		e.target.closest('li').remove();
		inputPrice.value = '';
	});

	return delBtn;
}

inputPrice.addEventListener('focus', focusFunc);
inputPrice.addEventListener('blur', blurFunc);
