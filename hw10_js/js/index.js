const inputConfirmPassword = document.querySelector('.input-confirm-password');
const inputPass = document.querySelector('#password');
const confirmPass = document.querySelector('#confirm');
const btn = document.querySelector('.btn');
const message = document.createElement('p');

const showOrHidePassword = (e) => {
	const target = e.target;

	if (target.classList.contains('icon-password')) {
		target.classList.toggle('fa-eye-slash');
		const input = target.parentElement.querySelector('.input-password');
		if (target.classList.contains('fa-eye-slash')) {
			input.type = 'text';
		} else {
			input.type = 'password';
		}
	}
};

const comparePassword = (e) => {
	e.preventDefault();
	if (!inputPass.value.trim() && !confirmPass.value.trim()) {
		return;
	}

	if (inputPass.value.trim() !== confirmPass.value.trim()) {
		message.innerHTML = 'Нужно ввести одинаковые значения';
		confirmPass.after(message);
		message.classList.add('error-message');
	} else {
		alert('You are welcome');
        message.remove();
	}
};

inputConfirmPassword.addEventListener('click', showOrHidePassword);
btn.addEventListener('click', comparePassword);
