const showList = (array, parent = document.body) => {
	const list = document.createElement('ul');
	const makeList = array.map(item => `<li>${item}</li>`);
	list.insertAdjacentHTML("beforeend", makeList.join(""));
	parent.append(list);
};

showList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
