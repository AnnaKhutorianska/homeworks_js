function isNumber(message) {
    let number = "";
    do {
        number = prompt(message);
    } while (!number || !number.trim() || Number.isNaN(+number))
    return +number;
}

function isMathOperation() {
    let mathOper = "";
    do {
        mathOper = prompt("input math operation");
    } while (mathOper !== '+' && mathOper !== '-' && mathOper !== '*' && mathOper !== '/')
    return mathOper;
}

let number1 = isNumber('input number 1');
let number2 = isNumber('input number 2');
let operation = isMathOperation();


function calculate(num1, num2, oper) {
    switch(oper) {
        case '+': 
            return num1 + num2;
        case '-': 
            return num1 - num2;
        case '*': 
            return num1 * num2;
        case '/': 
            if(num2 === 0){
                return 'cannot divide by 0';
            }
            return num1 / num2;
        default:
            return 'error';     
    }
}

const result = calculate(number1, number2, operation);
console.log(`${number1} ${operation} ${number2} = ${result}`);