const images = document.querySelectorAll('.image-to-show');
const btnPause = document.querySelector('#pause');
const btnContinue = document.querySelector('#continue');

let isPaused = false;
let imgIndx = 1;
let prevImgIndx = 0;

const showImage = () => {
	if (!isPaused) {
		images[imgIndx].classList.remove('hide-image');
		images[prevImgIndx].classList.add('hide-image');

		prevImgIndx = imgIndx;
		imgIndx++;

		if (imgIndx === images.length) {
			imgIndx = 0;
		}
	}
};

const slider = setInterval(showImage, 3000);

btnPause.addEventListener('click', () => (isPaused = true));
btnContinue.addEventListener('click', () => (isPaused = false));
