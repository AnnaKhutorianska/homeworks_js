const filterBy = (arr, type) => {
	if (type === 'null') {
		return arr.filter((elem) => elem !== null);
	}

	return arr.filter((elem) => elem === null || typeof elem !== type);
};

const inputArr = ['hello', 'world', 23, '23', null, undefined, NaN, [], {}, false];
const inputType = 'null';

console.log(filterBy(inputArr, inputType));

