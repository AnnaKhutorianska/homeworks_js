function createNewUser() {
  const firstName = inputName("Input first name");
  const lastName = inputName("Input last name");

  const newUser = {
    firstName,
    lastName,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    set setFirstName(valueName) {
      this.firstName = valueName;
    },
    set setLastName(valueLastName) {
      this.lastName = valueLastName;
    },
  };

  return newUser;
}

function inputName(message) {
  let name = "";
  do {
    name = prompt(message, name);
  } while (!name || !name.trim());

  return name;
}

const user = createNewUser();
user.setFirstName = "Ivan";
user.setLastName = "Kravchenko";
console.log(user);
console.log(user.getLogin());
