console.log(new Date().toISOString());

function createNewUser() {
	const firstName = inputName('Input first name');
	const lastName = inputName('Input last name');
	const birthday = inputDate('Input birthday');

	const newUser = {
		firstName,
		lastName,
		birthday,
		getLogin: function () {
			return (this.firstName[0] + this.lastName).toLowerCase();
		},
		getAge: function () {
			const now = new Date();
			const age = (now - this.birthday) / (24 * 3600 * 365.25 * 1000);

			return age > 0 ? Math.floor(age) : 0;
		},
		getPassword: function () {
			return (
				this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear()
			);
		},
		set setFirstName(valueName) {
			this.firstName = valueName;
		},
		set setLastName(valueLastName) {
			this.lastName = valueLastName;
		},
	};

	return newUser;
}

function inputName(message) {
	let name = '';
	do {
		name = prompt(message, name);
	} while (!name || !name.trim());

	return name;
}

function inputDate(message) {
	let date = '';
	const dateRegex = /^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$/;

	do {
		date = prompt(message, 'dd.mm.yyyy');
	} while (!date || !date.match(dateRegex));

	const [day, month, year] = date.split('.');

	return new Date(year, month - 1, day);
}

const user = createNewUser();
user.setFirstName = 'Ivan';
user.setLastName = 'Kravchenko';
console.log(user);
console.log(`user login - ${user.getLogin()}`);
console.log(`user age - ${user.getAge()}`);
console.log(`user password - ${user.getPassword()}`);
